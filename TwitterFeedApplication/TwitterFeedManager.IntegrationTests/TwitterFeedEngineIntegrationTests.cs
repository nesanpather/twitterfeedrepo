﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DataManager;
using DataManager.Interfaces;
using NUnit.Framework;
using TwitterFeedManager.Interfaces;
using TwitterFeedModels;
using Utilities;

namespace TwitterFeedManager.IntegrationTests
{
    [TestFixture]
    public class TwitterFeedEngineIntegrationTests
    {
        private string GetFullFilePathForIntegrationTest(string fileName)
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);

            if (!string.IsNullOrWhiteSpace(path))
            {
                var dir = Directory.GetParent(new UriBuilder(path).Path).Parent;
                var sb = new StringBuilder();
                if (dir != null) sb.Append(dir.FullName);
                sb.Append("\\");
                sb.Append(fileName);

                return sb.ToString();
            }

            return fileName;
        }

        [Test]
        public async Task GetTwitterFeedAsync_Given_ValidInput1_ValidInput2_3Users_3Tweets_Returns_ValidIEnumerableTwitterFeed_Count2()
        {
            // Arrange.
            var fileManager = new TextFileManager();
            var appSettings = new AppSettingsWrapper();
            var deserializer = new TextFileDeserializer(appSettings);
            var dataManager = new TextDataManager(fileManager, deserializer);
            var usersBuilder = new StandardTwitterUsersBuilder();
            var tweetBuilder = new StandardTweetBuilder();

            var twitterFeedEngine = new TwitterFeedEngine(dataManager, usersBuilder, tweetBuilder);            

            // Act.
            var actual = await twitterFeedEngine.GetTwitterFeedAsync(GetFullFilePathForIntegrationTest("testuser.txt"), GetFullFilePathForIntegrationTest("testtweet.txt"));

            // Assert.
            var twitterFeeds = actual as IList<TwitterFeed> ?? actual.ToList();

            Assert.AreEqual(2, twitterFeeds.Count);
            Assert.AreEqual("Alan", twitterFeeds.ToList()[0].User.UserName);

            Assert.AreEqual("Alan", twitterFeeds.ToList()[0].Tweets.ToList()[0].TwitterUser.UserName);
            Assert.AreEqual("If you have a procedure with 10 parameters, you probably missed some.", twitterFeeds.ToList()[0].Tweets.ToList()[0].Message);
            Assert.AreEqual("Alan", twitterFeeds.ToList()[0].Tweets.ToList()[1].TwitterUser.UserName);
            Assert.AreEqual("Random numbers should not be generated with a method chosen at random.", twitterFeeds.ToList()[0].Tweets.ToList()[1].Message);

            Assert.AreEqual("Martin", twitterFeeds.ToList()[0].Followers.ToList()[0].UserName);

            Assert.AreEqual("Ward", twitterFeeds.ToList()[1].User.UserName);

            Assert.AreEqual("Alan", twitterFeeds.ToList()[1].Tweets.ToList()[0].TwitterUser.UserName);
            Assert.AreEqual("If you have a procedure with 10 parameters, you probably missed some.", twitterFeeds.ToList()[1].Tweets.ToList()[0].Message);
            Assert.AreEqual("Ward", twitterFeeds.ToList()[1].Tweets.ToList()[1].TwitterUser.UserName);
            Assert.AreEqual("There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.", twitterFeeds.ToList()[1].Tweets.ToList()[1].Message);
            Assert.AreEqual("Alan", twitterFeeds.ToList()[1].Tweets.ToList()[2].TwitterUser.UserName);
            Assert.AreEqual("Random numbers should not be generated with a method chosen at random.", twitterFeeds.ToList()[1].Tweets.ToList()[2].Message);

            Assert.AreEqual("Alan", twitterFeeds.ToList()[1].Followers.ToList()[0].UserName);
            Assert.AreEqual("Martin", twitterFeeds.ToList()[1].Followers.ToList()[1].UserName);
        }
    }
}
