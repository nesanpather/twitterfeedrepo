﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataManager.Interfaces;
using NSubstitute;
using NUnit.Framework;
using TwitterFeedModels;
using Utilities.Interfaces;

namespace DataManager.Tests
{
    [TestFixture]
    public class TextDataManagerTests
    {
        [Test]
        public async Task GetTwitterUsersAsync_Given_ValidInput_ValidFileContents_ValidDeserialize_Returns_IEnumerableTwitterUser_NotNull()
        {
            // Arrange.
            var fileManager = Substitute.For<IFileManager>();
            var deserializer = Substitute.For<IDeserializer>();

            fileManager.GetFileContentsAsync(Arg.Any<string>())
                .Returns(info => Task.FromResult<IEnumerable<string>>(
                new List<string>
                {
                    "Ward follows Alan",
                    "Ward follows Martin, Alan"
                }));

            deserializer.DeserializeTwitterUsers(Arg.Any<IEnumerable<string>>())
                .Returns(info => new List<TwitterUser>
                {
                    new TwitterUser
                    {
                        Id = 1,
                        User = new User {UserName = "Ward"},
                        Followers = new List<User>
                        {
                            new User {UserName = "Alan"}
                        }
                    },
                    new TwitterUser
                    {
                        Id = 2,
                        User = new User {UserName = "Ward"},
                        Followers = new List<User>
                        {
                            new User {UserName = "Martin"},
                            new User {UserName = "Alan"}
                        }
                    }
                });

            var textDataManager = new TextDataManager(fileManager, deserializer);
            var input = "testfile.txt";

            // Act.
            var actual = await textDataManager.GetTwitterUsersAsync(input);

            // Assert.   
            Assert.IsNotNull(actual);
        }

        [Test]
        public async Task GetTwitterUsersAsync_Given_ValidInput_ValidFileContents_ValidDeserialize_Returns_IEnumerableTwitterUser_WithValidUserAndFollowers()
        {
            // Arrange.
            var fileManager = Substitute.For<IFileManager>();
            var deserializer = Substitute.For<IDeserializer>();

            fileManager.GetFileContentsAsync(Arg.Any<string>())
                .Returns(info => Task.FromResult<IEnumerable<string>>(
                    new List<string>
                    {
                        "Ward follows Alan",
                        "Ward follows Martin, Alan"
                    }));

            deserializer.DeserializeTwitterUsers(Arg.Any<IEnumerable<string>>())
                .Returns(info => new List<TwitterUser>
                {
                    new TwitterUser
                    {
                        Id = 1,
                        User = new User {UserName = "Ward"},
                        Followers = new List<User>
                        {
                            new User {UserName = "Alan"}
                        }
                    },
                    new TwitterUser
                    {
                        Id = 2,
                        User = new User {UserName = "Ward"},
                        Followers = new List<User>
                        {
                            new User {UserName = "Martin"},
                            new User {UserName = "Alan"}
                        }
                    }
                });

            var textDataManager = new TextDataManager(fileManager, deserializer);
            var input = "testfile.txt";

            // Act.
            var actual = await textDataManager.GetTwitterUsersAsync(input);

            // Assert.   
            var twitterUsers = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual(2, twitterUsers.Count);
            Assert.AreEqual("Ward", twitterUsers.ToList()[0].User.UserName);
            Assert.AreEqual("Alan", twitterUsers.ToList()[0].Followers.ToList()[0].UserName);

            Assert.AreEqual("Ward", twitterUsers.ToList()[1].User.UserName);
            Assert.AreEqual("Martin", twitterUsers.ToList()[1].Followers.ToList()[0].UserName);
            Assert.AreEqual("Alan", twitterUsers.ToList()[1].Followers.ToList()[1].UserName);
        }

        [Test]
        public async Task GetTwitterUsersAsync_Given_ValidInput_ValidEmptyFileContents_ValidEmptyDeserialize_Returns_IEnumerableTwitterUser_NotNullWithCount0()
        {
            // Arrange.
            var fileManager = Substitute.For<IFileManager>();
            var deserializer = Substitute.For<IDeserializer>();

            fileManager.GetFileContentsAsync(Arg.Any<string>())
                .Returns(info => Task.FromResult<IEnumerable<string>>(
                    new List<string>()));

            deserializer.DeserializeTwitterUsers(Arg.Any<IEnumerable<string>>())
                .Returns(info => new List<TwitterUser>());

            var textDataManager = new TextDataManager(fileManager, deserializer);
            var input = "testfile.txt";

            // Act.
            var actual = await textDataManager.GetTwitterUsersAsync(input);

            // Assert.   
            var twitterUsers = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.IsNotNull(actual);
            Assert.AreEqual(0, twitterUsers.Count);
        }

        [Test]
        public async Task GetTwitterUsersAsync_Given_NullValidInput_Returns_IEnumerableTwitterUser_NotNull()
        {
            // Arrange.
            var fileManager = Substitute.For<IFileManager>();
            var deserializer = Substitute.For<IDeserializer>();  

            var textDataManager = new TextDataManager(fileManager, deserializer);

            // Act.
            var actual = await textDataManager.GetTwitterUsersAsync(null);

            // Assert.   
            Assert.IsNotNull(actual);
        }

        [Test]
        public async Task GetTwitterUsersAsync_Given_EmptyValidInput_Returns_IEnumerableTwitterUser_NotNull()
        {
            // Arrange.
            var fileManager = Substitute.For<IFileManager>();
            var deserializer = Substitute.For<IDeserializer>();

            var textDataManager = new TextDataManager(fileManager, deserializer);

            // Act.
            var actual = await textDataManager.GetTwitterUsersAsync(" ");

            // Assert.   
            Assert.IsNotNull(actual);
        }

        [Test]
        public async Task GetTweetsAsync_Given_ValidInput_ValidFileContents_ValidDeserialize_Returns_IEnumerableTweet_NotNull()
        {
            // Arrange.
            var fileManager = Substitute.For<IFileManager>();
            var deserializer = Substitute.For<IDeserializer>();

            fileManager.GetFileContentsAsync(Arg.Any<string>())
                .Returns(info => Task.FromResult<IEnumerable<string>>(
                    new List<string>
                    {
                        "Alan> If you have a procedure with 10 parameters, you probably missed some."
                    }));

            deserializer.DeserializeTweets(Arg.Any<IEnumerable<string>>())
                .Returns(info => new List<Tweet>
                    {
                        new Tweet
                        {
                            Id = 1,
                            TwitterUser = new User {UserName = "Alan"},
                            Message = "If you have a procedure with 10 parameters, you probably missed some."
                        }
                    });

            var textDataManager = new TextDataManager(fileManager, deserializer);
            var input = "testfile2.txt";

            // Act.
            var actual = await textDataManager.GetTweetsAsync(input);

            // Assert.   
            Assert.IsNotNull(actual);
        }

        [Test]
        public async Task GetTweetsAsync_Given_ValidInput_ValidEmptyFileContents_ValidEmptyDeserialize_Returns_IEnumerableTweet_NotNullWithCount0()
        {
            // Arrange.
            var fileManager = Substitute.For<IFileManager>();
            var deserializer = Substitute.For<IDeserializer>();

            fileManager.GetFileContentsAsync(Arg.Any<string>())
                .Returns(info => Task.FromResult<IEnumerable<string>>(
                    new List<string>()));

            deserializer.DeserializeTweets(Arg.Any<IEnumerable<string>>())
                .Returns(info => new List<Tweet>());

            var textDataManager = new TextDataManager(fileManager, deserializer);
            var input = "testfile2.txt";

            // Act.
            var actual = await textDataManager.GetTweetsAsync(input);

            // Assert.   
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.Count());
        }

        [Test]
        public async Task GetTweetsAsync_Given_ValidInput_ValidFileContents_ValidDeserialize_Returns_IEnumerableTweet_WithValidTwitterUserAndMessage()
        {
            // Arrange.
            var fileManager = Substitute.For<IFileManager>();
            var deserializer = Substitute.For<IDeserializer>();

            fileManager.GetFileContentsAsync(Arg.Any<string>())
                .Returns(info => Task.FromResult<IEnumerable<string>>(
                    new List<string>
                    {
                        "Alan> If you have a procedure with 10 parameters, you probably missed some.",
                        "Ward> There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors."
                    }));

            deserializer.DeserializeTweets(Arg.Any<IEnumerable<string>>())
                .Returns(info => new List<Tweet>
                {
                    new Tweet
                    {
                        Id = 1,
                        TwitterUser = new User {UserName = "Alan"},
                        Message = "If you have a procedure with 10 parameters, you probably missed some."
                    },
                    new Tweet
                    {
                        Id = 2,
                        TwitterUser = new User {UserName = "Ward"},
                        Message = "There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors."
                    }
                });

            var textDataManager = new TextDataManager(fileManager, deserializer);
            var input = "testfile2.txt";

            // Act.
            var actual = await textDataManager.GetTweetsAsync(input);

            // Assert.   
            Assert.IsNotNull(actual);
            var enumerable = actual as IList<Tweet> ?? actual.ToList();

            Assert.AreEqual(2, enumerable.Count());
            Assert.AreEqual("Alan", enumerable.ToList()[0].TwitterUser.UserName);
            Assert.AreEqual("If you have a procedure with 10 parameters, you probably missed some.", enumerable.ToList()[0].Message);

            Assert.AreEqual("Ward", enumerable.ToList()[1].TwitterUser.UserName);
            Assert.AreEqual("There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.", enumerable.ToList()[1].Message);
        }

        [Test]
        public async Task GetTweetsAsync_Given_NullInput_Returns_IEnumerableTweet_NotNull()
        {
            // Arrange.
            var fileManager = Substitute.For<IFileManager>();
            var deserializer = Substitute.For<IDeserializer>();

            var textDataManager = new TextDataManager(fileManager, deserializer);

            // Act.
            var actual = await textDataManager.GetTweetsAsync(null);

            // Assert.   
            Assert.IsNotNull(actual);
        }

        [Test]
        public async Task GetTweetsAsync_Given_EmptyInput_Returns_IEnumerableTweet_NotNull()
        {
            // Arrange.
            var fileManager = Substitute.For<IFileManager>();
            var deserializer = Substitute.For<IDeserializer>();

            var textDataManager = new TextDataManager(fileManager, deserializer);

            // Act.
            var actual = await textDataManager.GetTweetsAsync(" ");

            // Assert.   
            Assert.IsNotNull(actual);
        }
    }
}
