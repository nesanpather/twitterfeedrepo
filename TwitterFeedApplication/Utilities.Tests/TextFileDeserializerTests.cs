﻿using System.Collections.Generic;
using System.Linq;
using NSubstitute;
using NUnit.Framework;
using TwitterFeedModels;
using Utilities.Interfaces;

namespace Utilities.Tests
{
    [TestFixture]
    public class TextFileDeserializerTests
    {
        private static void MockSettings(IAppSettings appSettings)
        {
            appSettings["followers.keyword"].Returns("follows");
            appSettings["followers.delimiter"].Returns(",");
            appSettings["tweet.delimiter"].Returns(">");
            appSettings["tweet.maxlength"].Returns("140");
        }

        [Test]
        public void DeserializeTwitterUsers_Given_1TwitterUserInput_EmptyString_Returns_NotNull_IEnumerableTwitterUsers_Count0()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var twitterUsers = new List<string>
            {
                ""
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTwitterUsers(twitterUsers);

            // Assert.
            Assert.NotNull(actual);
            Assert.AreEqual(0, actual.Count());
        }

        [Test]
        public void DeserializeTwitterUsers_Given_1TwitterUserInput_NullString_Returns_NotNull_IEnumerableTwitterUsers_Count0()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var twitterUsers = new List<string>
            {
                null
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTwitterUsers(twitterUsers);

            // Assert.
            Assert.NotNull(actual);
            Assert.AreEqual(0, actual.Count());
        }

        [Test]
        public void DeserializeTwitterUsers_Given_1TwitterUserInput_Returns_IEnumerableTwitterUsers_Count1()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);

            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var twitterUsers = new List<string>
            {
                "Ward follows Alan"
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTwitterUsers(twitterUsers);

            // Assert.
            Assert.AreEqual(1, actual.Count());
        }

        [Test]
        public void DeserializeTwitterUsers_Given_1TwitterUserInput_1UserWith1Follower_Returns_IEnumerableTwitterUsers_1UserWith1Follower()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var twitterUsers = new List<string>
            {
                "Ward follows Alan"
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTwitterUsers(twitterUsers);

            // Assert.
            var enumerable = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual("Ward", enumerable.ToList()[0].User.UserName);
            Assert.AreEqual("Alan", enumerable.ToList()[0].Followers.ToList()[0].UserName);
            Assert.AreEqual(1, enumerable[0].Id);
        }

        [Test]
        public void DeserializeTwitterUsers_Given_2TwitterUserInput_1UserWith1Follower_And_1UserWith1Follower_Returns_IEnumerableTwitterUsers_1UserWith1Follower_And_1UserWith1Follower()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var twitterUsers = new List<string>
            {
                "Ward follows Alan",
                "Alan follows Martin"
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTwitterUsers(twitterUsers);

            // Assert.
            var enumerable = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual("Ward", enumerable.ToList()[0].User.UserName);
            Assert.AreEqual("Alan", enumerable.ToList()[0].Followers.ToList()[0].UserName);
            Assert.AreEqual(1, enumerable[0].Id);
            Assert.AreEqual("Alan", enumerable.ToList()[1].User.UserName);
            Assert.AreEqual("Martin", enumerable.ToList()[1].Followers.ToList()[0].UserName);
            Assert.AreEqual(2, enumerable[1].Id);
        }

        [Test]
        public void DeserializeTwitterUsers_Given_1TwitterUserInput_1UserWith2Followers_Returns_IEnumerableTwitterUsers_1UserWith2Followers()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var twitterUsers = new List<string>
            {
                "Ward follows Martin, Alan"
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTwitterUsers(twitterUsers);

            // Assert.
            var enumerable = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual("Ward", enumerable.ToList()[0].User.UserName);
            Assert.AreEqual("Martin", enumerable.ToList()[0].Followers.ToList()[0].UserName);
            Assert.AreEqual("Alan", enumerable.ToList()[0].Followers.ToList()[1].UserName);
            Assert.AreEqual(1, enumerable[0].Id);
        }

        [Test]
        public void DeserializeTwitterUsers_Given_1TwitterUserInput_1UserWith5Followers_Returns_IEnumerableTwitterUsers_1UserWith5Followers()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var twitterUsers = new List<string>
            {
                "Ward follows Martin, Alan, Judy, Smith, Jane"
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTwitterUsers(twitterUsers);

            // Assert.
            var enumerable = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual("Ward", enumerable.ToList()[0].User.UserName);
            Assert.AreEqual("Martin", enumerable.ToList()[0].Followers.ToList()[0].UserName);
            Assert.AreEqual("Alan", enumerable.ToList()[0].Followers.ToList()[1].UserName);
            Assert.AreEqual("Judy", enumerable.ToList()[0].Followers.ToList()[2].UserName);
            Assert.AreEqual("Smith", enumerable.ToList()[0].Followers.ToList()[3].UserName);
            Assert.AreEqual("Jane", enumerable.ToList()[0].Followers.ToList()[4].UserName);
            Assert.AreEqual(1, enumerable[0].Id);
        }

        [Test]
        public void DeserializeTwitterUsers_Given_1TwitterUserInput_1UserWith5Followers_IrregularSpacingBetweenFollowers_Returns_IEnumerableTwitterUsers_1UserWith5Followers()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var twitterUsers = new List<string>
            {
                "Ward follows   Martin , Alan,Judy, Smith ,Jane "
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTwitterUsers(twitterUsers);

            // Assert.
            var enumerable = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual("Ward", enumerable.ToList()[0].User.UserName);
            Assert.AreEqual("Martin", enumerable.ToList()[0].Followers.ToList()[0].UserName);
            Assert.AreEqual("Alan", enumerable.ToList()[0].Followers.ToList()[1].UserName);
            Assert.AreEqual("Judy", enumerable.ToList()[0].Followers.ToList()[2].UserName);
            Assert.AreEqual("Smith", enumerable.ToList()[0].Followers.ToList()[3].UserName);
            Assert.AreEqual("Jane", enumerable.ToList()[0].Followers.ToList()[4].UserName);
            Assert.AreEqual(1, enumerable[0].Id);
        }

        [Test]
        public void DeserializeTwitterUsers_Given_1TwitterUserInput_1UserWith1Follower_UpperCaseF_Returns_IEnumerableTwitterUsers_1UserWith1Follower()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var twitterUsers = new List<string>
            {
                "Ward Follows Alan"
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTwitterUsers(twitterUsers);

            // Assert.
            var enumerable = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual("Ward", enumerable.ToList()[0].User.UserName);
            Assert.AreEqual("Alan", enumerable.ToList()[0].Followers.ToList()[0].UserName);
            Assert.AreEqual(1, enumerable[0].Id);
        }

        [Test]
        public void DeserializeTwitterUsers_Given_1TwitterUserInput_1UserWith1Follower_NoSpaces_Returns_IEnumerableTwitterUsers_1UserWith1Follower()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var twitterUsers = new List<string>
            {
                "WardfollowsAlan"
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTwitterUsers(twitterUsers);

            // Assert.
            var enumerable = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual("Ward", enumerable.ToList()[0].User.UserName);
            Assert.AreEqual("Alan", enumerable.ToList()[0].Followers.ToList()[0].UserName);
            Assert.AreEqual(1, enumerable[0].Id);
        }

        [Test]
        public void DeserializeTwitterUsers_Given_1TwitterUserInput_1UserWith2Follower_NoSpaces_Returns_IEnumerableTwitterUsers_1UserWith2Followers()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var twitterUsers = new List<string>
            {
                "WardfollowsAlan,Martin"
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTwitterUsers(twitterUsers);

            // Assert.
            var enumerable = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual("Ward", enumerable.ToList()[0].User.UserName);
            Assert.AreEqual("Alan", enumerable.ToList()[0].Followers.ToList()[0].UserName);
            Assert.AreEqual("Martin", enumerable.ToList()[0].Followers.ToList()[1].UserName);
            Assert.AreEqual(1, enumerable[0].Id);
        }

        [Test]
        public void DeserializeTwitterUsers_Given_1TwitterUserInput_1UserWith1Follower_IrregularCasingKeyWord_Returns_IEnumerableTwitterUsers_1UserWith1Follower()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var twitterUsers = new List<string>
            {
                "Ward fOLloWs Alan"
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTwitterUsers(twitterUsers);

            // Assert.
            var enumerable = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual("Ward", enumerable.ToList()[0].User.UserName);
            Assert.AreEqual("Alan", enumerable.ToList()[0].Followers.ToList()[0].UserName);
            Assert.AreEqual(1, enumerable[0].Id);
        }

        [Test]
        public void DeserializeTwitterUsers_Given_1TwitterUserInput_1User_EmptyFollower_Returns_IEnumerableTwitterUsers_1UserWithFollowerCount0()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var twitterUsers = new List<string>
            {
                "Ward"
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTwitterUsers(twitterUsers);

            // Assert.
            var enumerable = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual("Ward", enumerable.ToList()[0].User.UserName);
            Assert.AreEqual(0, enumerable.ToList()[0].Followers.ToList().Count);
            Assert.AreEqual(1, enumerable[0].Id);
        }

        [Test]
        public void DeserializeTwitterUsers_Given_1TwitterUserInput_1UserWith0Follower_Returns_IEnumerableTwitterUsers_1UserWithFollowerCount0()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var twitterUsers = new List<string>
            {
                "Ward follows"
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTwitterUsers(twitterUsers);

            // Assert.
            var enumerable = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual("Ward", enumerable.ToList()[0].User.UserName);
            Assert.AreEqual(0, enumerable.ToList()[0].Followers.ToList().Count);
            Assert.AreEqual(1, enumerable[0].Id);
        }

        [Test]
        public void DeserializeTwitterUsers_Given_1TwitterUserInput_0UserWith1Follower_Returns_IEnumerableTwitterUsers_0UserWithFollowerCount0()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var twitterUsers = new List<string>
            {
                "follows Martin"
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTwitterUsers(twitterUsers);

            // Assert.
            var enumerable = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual(null, enumerable.ToList()[0].User.UserName);
            Assert.AreEqual(0, enumerable.ToList()[0].Followers.ToList().Count);
        }

        [Test]
        public void DeserializeTwitterUsers_Given_NullInput_Returns_IEnumerableTwitterUsers_NotNullWithCount0()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);

            // Act.
            var actual = textFileDeserializer.DeserializeTwitterUsers(null);

            // Assert.
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.Count());
        }

        [Test]
        public void DeserializeTwitterUsers_Given_EmptyListInput_Returns_Empty_IEnumerableTwitterUsers()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var twitterUsers = new List<string>();

            // Act.
            var actual = textFileDeserializer.DeserializeTwitterUsers(twitterUsers);

            // Assert.
            Assert.AreEqual(0, actual.Count());
        }

        [Test]
        public void DeserializeTweets_Given_1Tweet_EmptyString_Returns_NotNull_IEnumerableTweets_Count0()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var tweets = new List<string>
            {
                ""
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTweets(tweets);

            // Assert.
            Assert.NotNull(actual);
            Assert.AreEqual(0, actual.Count());
        }

        [Test]
        public void DeserializeTweets_Given_1Tweet_NullString_Returns_NotNull_IEnumerableTweets_Count0()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var tweets = new List<string>
            {
                null
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTweets(tweets);

            // Assert.
            Assert.NotNull(actual);
            Assert.AreEqual(0, actual.Count());
        }

        [Test]
        public void DeserializeTweets_Given_NullTweet_Returns_NotNull_IEnumerableTweets_Count0()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);

            // Act.
            var actual = textFileDeserializer.DeserializeTweets(null);

            // Assert.
            Assert.NotNull(actual);
            Assert.AreEqual(0, actual.Count());
        }

        [Test]
        public void DeserializeTweets_Given_1TweetInput_Returns_IEnumerableTweets_Count1()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var tweets = new List<string>
            {
                "Alan> If you have a procedure with 10 parameters, you probably missed some."
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTweets(tweets);

            // Assert.
            var enumerable = actual as IList<Tweet> ?? actual.ToList();
            Assert.AreEqual(1, enumerable.Count);
            Assert.AreEqual(1, enumerable[0].Id);
        }

        [Test]
        public void DeserializeTweets_Given_1TweetInput_1User1Message_Returns_IEnumerableTweets_1UserWith1Tweet()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var tweets = new List<string>
            {
                "Alan> If you have a procedure with 10 parameters, you probably missed some."
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTweets(tweets);

            // Assert.
            var enumerable = actual as IList<Tweet> ?? actual.ToList();

            Assert.AreEqual("Alan", enumerable.ToList()[0].TwitterUser.UserName);
            Assert.AreEqual("If you have a procedure with 10 parameters, you probably missed some.", enumerable.ToList()[0].Message);
            Assert.AreEqual(1, enumerable[0].Id);
        }

        [Test]
        public void DeserializeTweets_Given_2TweetInput_1User1Message_And_1User1Message_Returns_IEnumerableTweets_1UserWith1Tweet_And_1UserWith1Tweet()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var tweets = new List<string>
            {
                "Alan> If you have a procedure with 10 parameters, you probably missed some.",
                "Ward> There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors."
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTweets(tweets);

            // Assert.
            var enumerable = actual as IList<Tweet> ?? actual.ToList();

            Assert.AreEqual("Alan", enumerable.ToList()[0].TwitterUser.UserName);
            Assert.AreEqual("If you have a procedure with 10 parameters, you probably missed some.", enumerable.ToList()[0].Message);
            Assert.AreEqual(1, enumerable[0].Id);
            Assert.AreEqual("Ward", enumerable.ToList()[1].TwitterUser.UserName);
            Assert.AreEqual("There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.", enumerable.ToList()[1].Message);
            Assert.AreEqual(2, enumerable[1].Id);
        }

        [Test]
        public void DeserializeTweets_Given_1TweetInput_1User1Message_140Characters_Returns_IEnumerableTweets_1UserWith1Tweet_140Characters()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var tweets = new List<string>
            {
                "Alan> If you have a procedure with 10 parameters, you probably missed some.If you have a procedure with 10 parameters, you probably missed some.If"
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTweets(tweets);

            // Assert.
            var enumerable = actual as IList<Tweet> ?? actual.ToList();

            Assert.AreEqual("Alan", enumerable.ToList()[0].TwitterUser.UserName);
            Assert.AreEqual("If you have a procedure with 10 parameters, you probably missed some.If you have a procedure with 10 parameters, you probably missed some.If", enumerable.ToList()[0].Message);
            Assert.AreEqual(1, enumerable[0].Id);
        }

        [Test]
        public void DeserializeTweets_Given_1TweetInput_1User1Message_139Characters_Returns_IEnumerableTweets_1UserWith1Tweet_139Characters()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var tweets = new List<string>
            {
                "Alan> If you have a procedure with 10 parameters, you probably missed some.If you have a procedure with 10 parameters, you probably missed some.I"
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTweets(tweets);

            // Assert.
            var enumerable = actual as IList<Tweet> ?? actual.ToList();

            Assert.AreEqual("Alan", enumerable.ToList()[0].TwitterUser.UserName);
            Assert.AreEqual("If you have a procedure with 10 parameters, you probably missed some.If you have a procedure with 10 parameters, you probably missed some.I", enumerable.ToList()[0].Message);
            Assert.AreEqual(1, enumerable[0].Id);
        }

        [Test]
        public void DeserializeTweets_Given_1TweetInput_1User1Message_141Characters_Returns_IEnumerableTweets_1UserWith1Tweet_140Characters()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var tweets = new List<string>
            {
                "Alan> If you have a procedure with 10 parameters, you probably missed some.If you have a procedure with 10 parameters, you probably missed some.Ify"
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTweets(tweets);

            // Assert.
            var enumerable = actual as IList<Tweet> ?? actual.ToList();

            Assert.AreEqual("Alan", enumerable.ToList()[0].TwitterUser.UserName);
            Assert.AreEqual("If you have a procedure with 10 parameters, you probably missed some.If you have a procedure with 10 parameters, you probably missed some.If", enumerable.ToList()[0].Message);
            Assert.AreEqual(1, enumerable[0].Id);
        }

        [Test]
        public void DeserializeTweets_Given_1TweetInput_1UserEmptyMessage_Returns_IEnumerableTweets_Count0()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var tweets = new List<string>
            {
                "Alan> "
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTweets(tweets);

            // Assert.
            Assert.NotNull(actual);
            var enumerable = actual as IList<Tweet> ?? actual.ToList();
            Assert.AreEqual(0, enumerable.Count);
        }

        [Test]
        public void DeserializeTweets_Given_1TweetInput_1User0Message_Returns_IEnumerableTweets_Count0()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var tweets = new List<string>
            {
                "Alan>"
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTweets(tweets);

            // Assert.
            Assert.NotNull(actual);
            Assert.AreEqual(0, actual.Count());
        }

        [Test]
        public void DeserializeTweets_Given_1TweetInput_0User1Message_Returns_IEnumerableTweets_Count0()
        {
            // Arrange.
            var appSettings = Substitute.For<IAppSettings>();
            MockSettings(appSettings);
            var textFileDeserializer = new TextFileDeserializer(appSettings);
            var tweets = new List<string>
            {
                "> If you have a procedure with 10 parameters, you probably missed some."
            };

            // Act.
            var actual = textFileDeserializer.DeserializeTweets(tweets);

            // Assert.
            Assert.NotNull(actual);
            Assert.AreEqual(0, actual.Count());
        }
    }
}
