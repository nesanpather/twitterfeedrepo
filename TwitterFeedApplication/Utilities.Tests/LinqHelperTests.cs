﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TwitterFeedModels;

namespace Utilities.Tests
{
    [TestFixture]
    public class LinqHelperTests
    {
        [Test]
        public void DistinctBy_Given_ValidIEnumerableSource_ValidFunc_Returns_IEnumerableDisctinct_Count1()
        {
            // Arrange.
            var source = new List<TwitterUser>
            {
                new TwitterUser
                {
                    Id = 1,
                    User = new User { UserName = "Ward"},
                    Followers = new List<User>
                    {
                        new User { UserName = "Alan"}
                    }
                },
                new TwitterUser
                {
                    Id = 2,
                    User = new User { UserName = "Ward"},
                    Followers = new List<User>
                    {
                        new User { UserName = "Alan"}
                    }
                }
            };

            // Act.
            var actual = LinqHelpers.DistinctBy(source, user => user.User.UserName);

            // Assert.
            Assert.AreEqual(1, actual.Count());
        }

        [Test]
        public void DistinctBy_Given_ValidEmptyIEnumerableSource_ValidFunc_Returns_IEnumerableDisctinct_Count0()
        {
            // Arrange.
            var source = new List<TwitterUser>();

            // Act.
            var actual = LinqHelpers.DistinctBy(source, user => user.User.UserName);

            // Assert.
            Assert.AreEqual(0, actual.Count());
        }
    }
}
