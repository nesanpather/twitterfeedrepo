﻿using NUnit.Framework;

namespace Utilities.Tests
{
    [TestFixture]
    public class AppSettingsWrapperTests
    {
        [Test]
        public void GetAppSettings_GivenKey_Returns_SettingValue()
        {
            // Arrange.
            var appSettings = new AppSettingsWrapper();

            // Act.
            var actual = appSettings["app.test"];

            // Assert.
            Assert.AreEqual("Utilities", actual);
        }

        [Test]
        public void GetAppSettings_GivenEmptyKey_Returns_Null()
        {
            // Arrange.
            var appSettings = new AppSettingsWrapper();

            // Act.
            var actual = appSettings[""];

            // Assert.
            Assert.IsNull(actual);
        }
    }
}
