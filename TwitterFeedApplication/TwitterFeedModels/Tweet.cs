﻿namespace TwitterFeedModels
{
    public class Tweet
    {
        public int Id { get; set; }
        public User TwitterUser { get; set; }
        public string Message { get; set; }
    }
}
