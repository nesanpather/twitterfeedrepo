﻿using System.Collections.Generic;

namespace TwitterFeedModels
{
    public class TwitterFeed
    {
        public User User { get; set; }
        public IEnumerable<Tweet> Tweets { get; set; }
        public IEnumerable<User> Followers { get; set; }       
    }
}
