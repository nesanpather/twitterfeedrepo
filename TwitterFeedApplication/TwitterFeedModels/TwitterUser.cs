﻿using System.Collections.Generic;

namespace TwitterFeedModels
{
    public class TwitterUser
    {
        public int Id { get; set; }
        public User User { get; set; }
        public IEnumerable<User> Followers { get; set; }
    }
}
