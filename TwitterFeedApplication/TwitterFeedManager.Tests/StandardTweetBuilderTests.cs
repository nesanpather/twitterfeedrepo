﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TwitterFeedModels;

namespace TwitterFeedManager.Tests
{
    [TestFixture]
    public class StandardTweetBuilderTests
    {
        [Test]
        public void SortTweets_GivenValidIEnumerableTwitterFeed_Returns_ListTwitterFeedSorted()
        {
            // Arrange.
            var standardTweetBuilder = new StandardTweetBuilder();

            var input = new List<TwitterFeed>
            {

                new TwitterFeed
                {
                    User = new User {UserName = "Alan"},
                    Followers = new List<User>
                    {
                        new User {UserName = "Martin"}
                    },
                    Tweets = new List<Tweet>
                    {
                        new Tweet
                        {
                            Id = 1,
                            TwitterUser = new User {UserName = "Alan"},
                            Message = "If you have a procedure with 10 parameters, you probably missed some."
                        },
                        new Tweet
                        {
                            Id = 3,
                            TwitterUser = new User {UserName = "Alan"},
                            Message = "Random numbers should not be generated with a method chosen at random."
                        }
                    }
                },
                new TwitterFeed
                {
                    User = new User {UserName = "Ward"},
                    Followers = new List<User>
                    {
                        new User {UserName = "Alan"},
                        new User {UserName = "Martin"}
                    },
                    Tweets = new List<Tweet>
                    {
                        new Tweet
                        {
                            Id = 3,
                            TwitterUser = new User {UserName = "Alan"},
                            Message = "Random numbers should not be generated with a method chosen at random."
                        },
                        new Tweet
                        {
                            Id = 1,
                            TwitterUser = new User {UserName = "Alan"},
                            Message = "If you have a procedure with 10 parameters, you probably missed some."
                        },
                        new Tweet
                        {
                            Id = 2,
                            TwitterUser = new User {UserName = "Ward"},
                            Message = "There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors."
                        }
                    }
                }};

            // Act.
            var actual = standardTweetBuilder.SortTweets(input);

            // Assert.
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual("If you have a procedure with 10 parameters, you probably missed some.", actual[1].Tweets.ToList()[0].Message);
            Assert.AreEqual("There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.", actual[1].Tweets.ToList()[1].Message);
            Assert.AreEqual("Random numbers should not be generated with a method chosen at random.", actual[1].Tweets.ToList()[2].Message);
        }

        [Test]
        public void SortTweets_GivenValidIEnumerableEmptyTwitterFeed_Returns_ListTwitterFeed_Count0()
        {
            // Arrange.
            var standardTweetBuilder = new StandardTweetBuilder();

            var input = new List<TwitterFeed>();

            // Act.
            var actual = standardTweetBuilder.SortTweets(input);

            // Assert.
            Assert.AreEqual(0, actual.Count);
        }
    }
}
