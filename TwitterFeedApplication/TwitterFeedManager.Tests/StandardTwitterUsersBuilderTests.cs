﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TwitterFeedModels;

namespace TwitterFeedManager.Tests
{
    [TestFixture]
    public class StandardTwitterUsersBuilderTests
    {
        [Test]
        public void SortUsers_Given_ValidIEnumerableTwitterUser_3Users_Returns_ValidIEnumerableTwitterUser_UsersSorted()
        {
            // Arrange.
            var standardTwitterUsersBuilder = new StandardTwitterUsersBuilder();
            var input = new List<TwitterUser>
            {
                new TwitterUser
                {
                    Id = 1,
                    User = new User { UserName = "Ward"},
                    Followers = new List<User>()
                },
                new TwitterUser
                {
                    Id = 2,
                    User = new User { UserName = "Alan"},
                    Followers = new List<User>()
                },
                new TwitterUser
                {
                    Id = 3,
                    User = new User { UserName = "Martin"},
                    Followers = new List<User>()
                }
            };

            // Act.
            var actual = standardTwitterUsersBuilder.SortUsers(input);

            // Assert.
            var twitterUsers = actual as IList<TwitterUser> ?? actual.ToList();
            
            Assert.AreEqual("Alan", twitterUsers[0].User.UserName);
            Assert.AreEqual("Martin", twitterUsers[1].User.UserName);
            Assert.AreEqual("Ward", twitterUsers[2].User.UserName);
        }

        [Test]
        public void SortUsers_Given_ValidEmptyIEnumerableTwitterUser_Returns_ValidIEnumerableTwitterUser_Count0()
        {
            // Arrange.
            var standardTwitterUsersBuilder = new StandardTwitterUsersBuilder();
            var input = new List<TwitterUser>();

            // Act.
            var actual = standardTwitterUsersBuilder.SortUsers(input);

            // Assert.
            var twitterUsers = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual(0, twitterUsers.Count);
        }

        [Test]
        public void SortFollowers_Given_ValidIEnumerableTwitterUser_3Users_1FollowerEach_Returns_ValidIEnumerableTwitterUser_UsersAndFollowersUnSorted()
        {
            // Arrange.
            var standardTwitterUsersBuilder = new StandardTwitterUsersBuilder();
            var input = new List<TwitterUser>
            {
                new TwitterUser
                {
                    Id = 1,
                    User = new User { UserName = "Ward"},
                    Followers = new List<User>
                    {
                        new User { UserName = "James" }
                    }
                },
                new TwitterUser
                {
                    Id = 2,
                    User = new User { UserName = "Alan"},
                    Followers = new List<User>
                    {
                        new User { UserName = "Ward" }
                    }
                },
                new TwitterUser
                {
                    Id = 3,
                    User = new User { UserName = "Martin"},
                    Followers = new List<User>
                    {
                        new User { UserName = "Danny" }
                    }
                }
            };

            // Act.
            var actual = standardTwitterUsersBuilder.SortFollowers(input);

            // Assert.
            var twitterUsers = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual("Ward", twitterUsers[0].User.UserName);
            Assert.AreEqual("James", twitterUsers[0].Followers.ToList()[0].UserName);
            Assert.AreEqual("Alan", twitterUsers[1].User.UserName);
            Assert.AreEqual("Ward", twitterUsers[1].Followers.ToList()[0].UserName);
            Assert.AreEqual("Martin", twitterUsers[2].User.UserName);
            Assert.AreEqual("Danny", twitterUsers[2].Followers.ToList()[0].UserName);
        }

        [Test]
        public void SortFollowers_Given_ValidIEnumerableTwitterUser_2Users_3FollowerEach_Returns_ValidIEnumerableTwitterUser_FollowersSorted()
        {
            // Arrange.
            var standardTwitterUsersBuilder = new StandardTwitterUsersBuilder();
            var input = new List<TwitterUser>
            {
                new TwitterUser
                {
                    Id = 1,
                    User = new User { UserName = "Ward"},
                    Followers = new List<User>
                    {
                        new User { UserName = "James" },
                        new User { UserName = "Alan" },
                        new User { UserName = "Bruce" }
                    }
                },
                new TwitterUser
                {
                    Id = 2,
                    User = new User { UserName = "Alan"},
                    Followers = new List<User>
                    {
                        new User { UserName = "Ward" },
                        new User { UserName = "Bruce" },
                        new User { UserName = "Kate" }
                    }
                }
            };

            // Act.
            var actual = standardTwitterUsersBuilder.SortFollowers(input);

            // Assert.
            var twitterUsers = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual("Ward", twitterUsers[0].User.UserName);
            Assert.AreEqual("Alan", twitterUsers[0].Followers.ToList()[0].UserName);
            Assert.AreEqual("Bruce", twitterUsers[0].Followers.ToList()[1].UserName);
            Assert.AreEqual("James", twitterUsers[0].Followers.ToList()[2].UserName);

            Assert.AreEqual("Alan", twitterUsers[1].User.UserName);
            Assert.AreEqual("Bruce", twitterUsers[1].Followers.ToList()[0].UserName);
            Assert.AreEqual("Kate", twitterUsers[1].Followers.ToList()[1].UserName);
            Assert.AreEqual("Ward", twitterUsers[1].Followers.ToList()[2].UserName);
        }

        [Test]
        public void SortFollowers_Given_ValidEmptyIEnumerableTwitterUser_Returns_ValidIEnumerableTwitterUser_Count0()
        {
            // Arrange.
            var standardTwitterUsersBuilder = new StandardTwitterUsersBuilder();
            var input = new List<TwitterUser>();

            // Act.
            var actual = standardTwitterUsersBuilder.SortFollowers(input);

            // Assert.
            var twitterUsers = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual(0, twitterUsers.Count);
        }

        [Test]
        public void UnionDuplicateUsers_Given_ValidIEnumerableTwitterUser_3Users_Returns_ValidIEnumerableTwitterUser_Count2_UsersUnioned()
        {
            // Arrange.
            var standardTwitterUsersBuilder = new StandardTwitterUsersBuilder();
            var input = new List<TwitterUser>
            {
                new TwitterUser
                {
                    Id = 1,
                    User = new User { UserName = "Ward"},
                    Followers = new List<User>
                    {
                        new User { UserName = "Alan"}
                    }
                },
                new TwitterUser
                {
                    Id = 2,
                    User = new User { UserName = "Alan"},
                    Followers = new List<User>
                    {
                        new User { UserName = "Martin"}
                    }
                },
                new TwitterUser
                {
                    Id = 3,
                    User = new User { UserName = "Ward"},
                    Followers = new List<User>
                    {
                        new User { UserName = "Martin"},
                        new User { UserName = "Alan"}
                    }
                }
            };

            // Act.
            var actual = standardTwitterUsersBuilder.UnionDuplicateUsers(input);

            // Assert.
            var twitterUsers = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual(2, twitterUsers.Count);
            Assert.AreEqual("Ward", twitterUsers[0].User.UserName);
            Assert.AreEqual("Alan", twitterUsers[0].Followers.ToList()[0].UserName);
            Assert.AreEqual("Martin", twitterUsers[0].Followers.ToList()[1].UserName);

            Assert.AreEqual("Alan", twitterUsers[1].User.UserName);
            Assert.AreEqual("Martin", twitterUsers[1].Followers.ToList()[0].UserName);
        }

        [Test]
        public void UnionDuplicateUsers_Given_ValidIEnumerableTwitterUser_2DuplicateUsersAndFollowers_Returns_ValidIEnumerableTwitterUser_Count1_UsersUnioned()
        {
            // Arrange.
            var standardTwitterUsersBuilder = new StandardTwitterUsersBuilder();
            var input = new List<TwitterUser>
            {
                new TwitterUser
                {
                    Id = 1,
                    User = new User { UserName = "Ward"},
                    Followers = new List<User>
                    {
                        new User { UserName = "Alan"}
                    }
                },
                new TwitterUser
                {
                    Id = 2,
                    User = new User { UserName = "Ward"},
                    Followers = new List<User>
                    {
                        new User { UserName = "Alan"}
                    }
                }
            };

            // Act.
            var actual = standardTwitterUsersBuilder.UnionDuplicateUsers(input);

            // Assert.
            var twitterUsers = actual as IList<TwitterUser> ?? actual.ToList();

            Assert.AreEqual(1, twitterUsers.Count);
            Assert.AreEqual("Ward", twitterUsers[0].User.UserName);
            Assert.AreEqual("Alan", twitterUsers[0].Followers.ToList()[0].UserName);
        }
    }
}
