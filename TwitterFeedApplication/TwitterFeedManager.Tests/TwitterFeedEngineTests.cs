﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataManager.Interfaces;
using NSubstitute;
using NUnit.Framework;
using TwitterFeedManager.Interfaces;
using TwitterFeedModels;

namespace TwitterFeedManager.Tests
{
    [TestFixture]
    public class TwitterFeedEngineTests
    {
        [Test]
        public async Task GetTwitterFeedAsync_Given_ValidInput1_ValidInput2_3Users_3Tweets_Returns_ValidIEnumerableTwitterFeed_Count2()
        {
            // Arrange.
            var dataManager = Substitute.For<IDataManager>();
            var twitterUsersBuilder = Substitute.For<ITwitterUsersBuilder>();
            var twitterFeedBuilder = Substitute.For<ITweetBuilder>();

            dataManager.GetTwitterUsersAsync(Arg.Any<string>())
                .Returns(info => Task.FromResult<IEnumerable<TwitterUser>>(
                    new List<TwitterUser>
                    {
                        new TwitterUser
                        {
                            User = new User {UserName = "Ward"},
                            Followers = new List<User>
                            {
                                new User {UserName = "Alan"}
                            }
                        },
                        new TwitterUser
                        {
                            User = new User {UserName = "Alan"},
                            Followers = new List<User>
                            {
                                new User {UserName = "Martin"}
                            }
                        },
                        new TwitterUser
                        {
                            User = new User {UserName = "Ward"},
                            Followers = new List<User>
                            {
                                new User {UserName = "Martin"},
                                new User {UserName = "Alan"}
                            }
                        }
                    }));

            dataManager.GetTweetsAsync(Arg.Any<string>())
                .Returns(info => Task.FromResult<IEnumerable<Tweet>>(new List<Tweet>
                {
                    new Tweet
                    {
                        Id = 1,
                        TwitterUser = new User {UserName = "Alan"},
                        Message = "If you have a procedure with 10 parameters, you probably missed some."
                    },
                    new Tweet
                    {
                        Id = 2,
                        TwitterUser = new User {UserName = "Ward"},
                        Message = "There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors."
                    },
                    new Tweet
                    {
                        Id = 3,
                        TwitterUser = new User {UserName = "Alan"},
                        Message = "Random numbers should not be generated with a method chosen at random."
                    }
                }));

            twitterUsersBuilder.UnionDuplicateUsers(Arg.Any<IEnumerable<TwitterUser>>())
                .Returns(info => new List<TwitterUser>
                {
                    new TwitterUser
                    {
                        User = new User {UserName = "Ward"},
                        Followers = new List<User>
                        {
                            new User {UserName = "Alan"},
                            new User {UserName = "Martin"}
                        }
                    },
                    new TwitterUser
                    {
                        User = new User {UserName = "Alan"},
                        Followers = new List<User>
                        {
                            new User {UserName = "Martin"}
                        }
                    }
                });

            twitterUsersBuilder.SortUsers(Arg.Any<IEnumerable<TwitterUser>>())
                .Returns(info => new List<TwitterUser>
                {
                    new TwitterUser
                    {
                        User = new User {UserName = "Alan"},
                        Followers = new List<User>
                        {
                            new User {UserName = "Martin"}
                        }
                    },
                    new TwitterUser
                    {
                        User = new User {UserName = "Ward"},
                        Followers = new List<User>
                        {
                            new User {UserName = "Alan"},
                            new User {UserName = "Martin"}
                        }
                    }
                });

            twitterUsersBuilder.SortFollowers(Arg.Any<IEnumerable<TwitterUser>>())
                .Returns(info => new List<TwitterUser>
                {
                    new TwitterUser
                    {
                        User = new User {UserName = "Alan"},
                        Followers = new List<User>
                        {
                            new User {UserName = "Martin"}
                        }
                    },
                    new TwitterUser
                    {
                        User = new User {UserName = "Ward"},
                        Followers = new List<User>
                        {
                            new User {UserName = "Alan"},
                            new User {UserName = "Martin"}
                        }
                    }
                });

            twitterFeedBuilder.SortTweets(Arg.Any<IEnumerable<TwitterFeed>>()).Returns(info => new List<TwitterFeed>
            {
               new TwitterFeed
               {
                   User = new User {UserName = "Alan"},
                   Followers = new List<User>
                   {
                       new User {UserName = "Martin"}
                   },
                   Tweets = new List<Tweet>
                   {
                       new Tweet
                       {
                           Id = 1,
                           TwitterUser = new User {UserName = "Alan"},
                           Message = "If you have a procedure with 10 parameters, you probably missed some."
                       },
                       new Tweet
                       {
                           Id = 3,
                           TwitterUser = new User {UserName = "Alan"},
                           Message = "Random numbers should not be generated with a method chosen at random."
                       }
                   }
               },
                new TwitterFeed
                {
                    User = new User {UserName = "Ward"},
                    Followers = new List<User>
                    {
                        new User {UserName = "Alan"},
                        new User {UserName = "Martin"}
                    },
                    Tweets = new List<Tweet>
                    {
                        new Tweet
                        {
                            Id = 1,
                            TwitterUser = new User {UserName = "Alan"},
                            Message = "If you have a procedure with 10 parameters, you probably missed some."
                        },
                        new Tweet
                        {
                            Id = 2,
                            TwitterUser = new User {UserName = "Ward"},
                            Message = "There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors."
                        },
                        new Tweet
                        {
                            Id = 3,
                            TwitterUser = new User {UserName = "Alan"},
                            Message = "Random numbers should not be generated with a method chosen at random."
                        }
                    }
                }
            });

            var twitterFeedEngine = new TwitterFeedEngine(dataManager, twitterUsersBuilder, twitterFeedBuilder);

            // Act.
            var actual = await twitterFeedEngine.GetTwitterFeedAsync("test1.txt", "test2.txt");

            // Assert.
            var twitterFeeds = actual as IList<TwitterFeed> ?? actual.ToList();

            Assert.AreEqual(2, twitterFeeds.Count);
            Assert.AreEqual("Alan", twitterFeeds.ToList()[0].User.UserName);

            Assert.AreEqual("Alan", twitterFeeds.ToList()[0].Tweets.ToList()[0].TwitterUser.UserName);
            Assert.AreEqual("If you have a procedure with 10 parameters, you probably missed some.", twitterFeeds.ToList()[0].Tweets.ToList()[0].Message);
            Assert.AreEqual("Alan", twitterFeeds.ToList()[0].Tweets.ToList()[1].TwitterUser.UserName);
            Assert.AreEqual("Random numbers should not be generated with a method chosen at random.", twitterFeeds.ToList()[0].Tweets.ToList()[1].Message);

            Assert.AreEqual("Martin", twitterFeeds.ToList()[0].Followers.ToList()[0].UserName);            

            Assert.AreEqual("Ward", twitterFeeds.ToList()[1].User.UserName);

            Assert.AreEqual("Alan", twitterFeeds.ToList()[1].Tweets.ToList()[0].TwitterUser.UserName);
            Assert.AreEqual("If you have a procedure with 10 parameters, you probably missed some.", twitterFeeds.ToList()[1].Tweets.ToList()[0].Message);
            Assert.AreEqual("Ward", twitterFeeds.ToList()[1].Tweets.ToList()[1].TwitterUser.UserName);
            Assert.AreEqual("There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.", twitterFeeds.ToList()[1].Tweets.ToList()[1].Message);
            Assert.AreEqual("Alan", twitterFeeds.ToList()[1].Tweets.ToList()[2].TwitterUser.UserName);
            Assert.AreEqual("Random numbers should not be generated with a method chosen at random.", twitterFeeds.ToList()[1].Tweets.ToList()[2].Message);

            Assert.AreEqual("Alan", twitterFeeds.ToList()[1].Followers.ToList()[0].UserName);
            Assert.AreEqual("Martin", twitterFeeds.ToList()[1].Followers.ToList()[1].UserName);
        }

        [Test]
        public async Task GetTwitterFeedAsync_Given_ValidInput1_ValidInput2_1User1Follower_3Tweets_Returns_ValidIEnumerableTwitterFeed_Count1()
        {
            // Arrange.
            var dataManager = Substitute.For<IDataManager>();
            var twitterUsersBuilder = Substitute.For<ITwitterUsersBuilder>();
            var twitterFeedBuilder = Substitute.For<ITweetBuilder>();

            dataManager.GetTwitterUsersAsync(Arg.Any<string>())
                .Returns(info => Task.FromResult<IEnumerable<TwitterUser>>(
                    new List<TwitterUser>
                    {
                        new TwitterUser
                        {
                            User = new User {UserName = "Ward"},
                            Followers = new List<User>
                            {
                                new User {UserName = "Alan"}
                            }
                        }
                    }));

            dataManager.GetTweetsAsync(Arg.Any<string>())
                .Returns(info => Task.FromResult<IEnumerable<Tweet>>(new List<Tweet>
                {
                    new Tweet
                    {
                        Id = 1,
                        TwitterUser = new User {UserName = "Alan"},
                        Message = "If you have a procedure with 10 parameters, you probably missed some."
                    },
                    new Tweet
                    {
                        Id = 2,
                        TwitterUser = new User {UserName = "Ward"},
                        Message = "There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors."
                    },
                    new Tweet
                    {
                        Id = 3,
                        TwitterUser = new User {UserName = "Alan"},
                        Message = "Random numbers should not be generated with a method chosen at random."
                    }
                }));

            twitterUsersBuilder.UnionDuplicateUsers(Arg.Any<IEnumerable<TwitterUser>>())
                .Returns(info => new List<TwitterUser>
                {
                    new TwitterUser
                    {
                        User = new User {UserName = "Ward"},
                        Followers = new List<User>
                        {
                            new User {UserName = "Alan"}
                        }
                    }
                });

            twitterUsersBuilder.SortUsers(Arg.Any<IEnumerable<TwitterUser>>())
                .Returns(info => new List<TwitterUser>
                {
                    new TwitterUser
                    {
                        User = new User {UserName = "Ward"},
                        Followers = new List<User>
                        {
                            new User {UserName = "Alan"}
                        }
                    }
                });

            twitterUsersBuilder.SortFollowers(Arg.Any<IEnumerable<TwitterUser>>())
                .Returns(info => new List<TwitterUser>
                {
                    new TwitterUser
                    {
                        User = new User {UserName = "Ward"},
                        Followers = new List<User>
                        {
                            new User {UserName = "Alan"}
                        }
                    }
                });

            twitterFeedBuilder.SortTweets(Arg.Any<IEnumerable<TwitterFeed>>()).Returns(info => new List<TwitterFeed>
            {
                new TwitterFeed
                {
                    User = new User {UserName = "Ward"},
                    Followers = new List<User>
                    {
                        new User {UserName = "Alan"}
                    },
                    Tweets = new List<Tweet>
                    {
                        new Tweet
                        {
                            Id = 1,
                            TwitterUser = new User {UserName = "Alan"},
                            Message = "If you have a procedure with 10 parameters, you probably missed some."
                        },
                        new Tweet
                        {
                            Id = 2,
                            TwitterUser = new User {UserName = "Ward"},
                            Message = "There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors."
                        },
                        new Tweet
                        {
                            Id = 3,
                            TwitterUser = new User {UserName = "Alan"},
                            Message = "Random numbers should not be generated with a method chosen at random."
                        }
                    }
                }
            });

            var twitterFeedEngine = new TwitterFeedEngine(dataManager, twitterUsersBuilder, twitterFeedBuilder);

            // Act.
            var actual = await twitterFeedEngine.GetTwitterFeedAsync("test1.txt", "test2.txt");

            // Assert.
            var twitterFeeds = actual as IList<TwitterFeed> ?? actual.ToList();

            Assert.AreEqual(1, twitterFeeds.Count);
            Assert.AreEqual("Ward", twitterFeeds.ToList()[0].User.UserName);

            Assert.AreEqual("Alan", twitterFeeds.ToList()[0].Tweets.ToList()[0].TwitterUser.UserName);
            Assert.AreEqual("If you have a procedure with 10 parameters, you probably missed some.", twitterFeeds.ToList()[0].Tweets.ToList()[0].Message);
            Assert.AreEqual("Ward", twitterFeeds.ToList()[0].Tweets.ToList()[1].TwitterUser.UserName);
            Assert.AreEqual("There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.", twitterFeeds.ToList()[0].Tweets.ToList()[1].Message);
            Assert.AreEqual("Alan", twitterFeeds.ToList()[0].Tweets.ToList()[2].TwitterUser.UserName);
            Assert.AreEqual("Random numbers should not be generated with a method chosen at random.", twitterFeeds.ToList()[0].Tweets.ToList()[2].Message);

            Assert.AreEqual("Alan", twitterFeeds.ToList()[0].Followers.ToList()[0].UserName);
        }


        [Test]
        public async Task GetTwitterFeedAsync_Given_ValidInput1_ValidInput2_1User0Follower_3Tweets_Returns_ValidIEnumerableTwitterFeed_Count1_Tweet()
        {
            // Arrange.
            var dataManager = Substitute.For<IDataManager>();
            var twitterUsersBuilder = Substitute.For<ITwitterUsersBuilder>();
            var twitterFeedBuilder = Substitute.For<ITweetBuilder>();

            dataManager.GetTwitterUsersAsync(Arg.Any<string>())
                .Returns(info => Task.FromResult<IEnumerable<TwitterUser>>(
                    new List<TwitterUser>
                    {
                        new TwitterUser
                        {
                            User = new User {UserName = "Ward"}
                        }
                    }));

            dataManager.GetTweetsAsync(Arg.Any<string>())
                .Returns(info => Task.FromResult<IEnumerable<Tweet>>(new List<Tweet>
                {
                    new Tweet
                    {
                        Id = 1,
                        TwitterUser = new User {UserName = "Alan"},
                        Message = "If you have a procedure with 10 parameters, you probably missed some."
                    },
                    new Tweet
                    {
                        Id = 2,
                        TwitterUser = new User {UserName = "Ward"},
                        Message = "There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors."
                    },
                    new Tweet
                    {
                        Id = 3,
                        TwitterUser = new User {UserName = "Alan"},
                        Message = "Random numbers should not be generated with a method chosen at random."
                    }
                }));

            twitterUsersBuilder.UnionDuplicateUsers(Arg.Any<IEnumerable<TwitterUser>>())
                .Returns(info => new List<TwitterUser>
                {
                    new TwitterUser
                    {
                        User = new User {UserName = "Ward"},
                        Followers = new List<User>()
                    }
                });

            twitterUsersBuilder.SortUsers(Arg.Any<IEnumerable<TwitterUser>>())
                .Returns(info => new List<TwitterUser>
                {
                    new TwitterUser
                    {
                        User = new User {UserName = "Ward"},
                        Followers = new List<User>()
                    }
                });

            twitterUsersBuilder.SortFollowers(Arg.Any<IEnumerable<TwitterUser>>())
                .Returns(info => new List<TwitterUser>
                {
                    new TwitterUser
                    {
                        User = new User {UserName = "Ward"},
                        Followers = new List<User>()
                    }
                });

            twitterFeedBuilder.SortTweets(Arg.Any<IEnumerable<TwitterFeed>>()).Returns(info => new List<TwitterFeed>
            {
                new TwitterFeed
                {
                    User = new User {UserName = "Ward"},
                    Followers = new List<User>(),
                    Tweets = new List<Tweet>
                    {
                        
                        new Tweet
                        {
                            Id = 2,
                            TwitterUser = new User {UserName = "Ward"},
                            Message = "There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors."
                        }
                    }
                }
            });

            var twitterFeedEngine = new TwitterFeedEngine(dataManager, twitterUsersBuilder, twitterFeedBuilder);

            // Act.
            var actual = await twitterFeedEngine.GetTwitterFeedAsync("test1.txt", "test2.txt");

            // Assert.
            var twitterFeeds = actual as IList<TwitterFeed> ?? actual.ToList();

            Assert.AreEqual(1, twitterFeeds.Count);
            Assert.AreEqual("Ward", twitterFeeds.ToList()[0].User.UserName);
            
            Assert.AreEqual("Ward", twitterFeeds.ToList()[0].Tweets.ToList()[0].TwitterUser.UserName);
            Assert.AreEqual("There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.", twitterFeeds.ToList()[0].Tweets.ToList()[0].Message);

            Assert.AreEqual(0, twitterFeeds.ToList()[0].Followers.Count());
        }

    }
}
