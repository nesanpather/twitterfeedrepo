﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataManager.Interfaces;
using Utilities;

namespace DataManager
{
    public class TextFileManager: IFileManager
    {
        public async Task<IEnumerable<string>> GetFileContentsAsync(string fullFileName)
        {
            var fileContents = new List<string>();

            if (string.IsNullOrWhiteSpace(fullFileName))
            {
                // File name is invalid.
                ConsoleLogger.SingleInstance.LogError("TextFileManager.GetFileContentsAsync - File name is invalid.");
                return fileContents;
            }

            try
            {
                using (var sr = new StreamReader(fullFileName, Encoding.ASCII))
                {
                    string readText;
                    while ((readText = await sr.ReadLineAsync()) != null)
                    {
                        fileContents.Add(readText);
                    }
                }
            }
            catch (Exception e)
            {
                // Error reading file.
                ConsoleLogger.SingleInstance.LogError($"TextFileManager.GetFileContentsAsync - Error reading file {fullFileName}");
                return fileContents;
            }

            
            return fileContents;
        }
    }
}
