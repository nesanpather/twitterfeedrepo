﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataManager.Interfaces;
using TwitterFeedModels;
using Utilities;
using Utilities.Interfaces;

namespace DataManager
{
    public class TextDataManager: IDataManager
    {
        private readonly IFileManager _fileManager;
        private readonly IDeserializer _deserializer;

        public TextDataManager(IFileManager fileManager, IDeserializer deserializer)
        {
            _fileManager = fileManager;
            _deserializer = deserializer;
        }

        public async Task<IEnumerable<TwitterUser>> GetTwitterUsersAsync(string input)
        {
            var twitterUsers = new List<TwitterUser>();

            if (string.IsNullOrWhiteSpace(input))
            {
                // Invalid string input.
                ConsoleLogger.SingleInstance.LogError("TextDataManager.GetTwitterUsersAsync - Invalid string input.");
                return twitterUsers;
            }

            var userFileContents = await _fileManager.GetFileContentsAsync(input);

            twitterUsers = _deserializer.DeserializeTwitterUsers(userFileContents).ToList();

            return twitterUsers;
        }

        public async Task<IEnumerable<Tweet>> GetTweetsAsync(string input)
        {
            var tweets = new List<Tweet>();

            if (string.IsNullOrWhiteSpace(input))
            {
                // Invalid string input.
                ConsoleLogger.SingleInstance.LogError("TextDataManager.GetTwitterUsersAsync - Invalid string input.");
                return tweets;
            }

            var userFileContents = await _fileManager.GetFileContentsAsync(input);

            tweets = _deserializer.DeserializeTweets(userFileContents).ToList();

            return tweets;
        }
    }
}
