﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TwitterFeedModels;

namespace DataManager.Interfaces
{
    public interface IDataManager
    {
        Task<IEnumerable<TwitterUser>> GetTwitterUsersAsync(string input);
        Task<IEnumerable<Tweet>> GetTweetsAsync(string input);
    }
}
