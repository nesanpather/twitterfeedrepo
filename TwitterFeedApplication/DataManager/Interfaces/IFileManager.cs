﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataManager.Interfaces
{
    public interface IFileManager
    {
        Task<IEnumerable<string>> GetFileContentsAsync(string fullFileName);
    }
}
