﻿using System;
using System.Collections.Generic;
using TwitterFeedModels;
using Utilities.Interfaces;

namespace Utilities
{
    public class TextFileDeserializer : IDeserializer
    {
        private readonly IAppSettings _appSettings;

        public TextFileDeserializer(IAppSettings appSettings)
        {
            _appSettings = appSettings;
        }

        public IEnumerable<TwitterUser> DeserializeTwitterUsers(IEnumerable<string> twitterUsers)
        {
            var deserializedTwitterUsers = new List<TwitterUser>();

            if (twitterUsers == null)
            {
                // TwitterUsers string is null.
                ConsoleLogger.SingleInstance.LogError("TextFileDeserializer.DeserializeTwitterUsers - Users string is null.");
                return deserializedTwitterUsers;
            }

            var userFollowerKeyWord = _appSettings["followers.keyword"];
            var id = 0;

            foreach (var twitterUser in twitterUsers)
            {
                if (string.IsNullOrWhiteSpace(twitterUser))
                {
                    // String is invalid.
                    ConsoleLogger.SingleInstance.LogError("TextFileDeserializer.DeserializeTwitterUsers - Users string is null.");
                    continue;
                }

                var indexOfKeyWord = twitterUser.IndexOf(userFollowerKeyWord, StringComparison.InvariantCultureIgnoreCase);
                id++;
                var newTwitterUser = new TwitterUser
                {
                    Id = id,
                    User = DeserializeUser(indexOfKeyWord, twitterUser),
                    Followers = new List<User>()
                };

                if (indexOfKeyWord <= 0)
                {
                    // No Followers or no User.
                    ConsoleLogger.SingleInstance.LogError("TextFileDeserializer.DeserializeTwitterUsers - No Followers or no User.");
                    deserializedTwitterUsers.Add(newTwitterUser);
                    continue;
                }

                var followersStartIndex = indexOfKeyWord + userFollowerKeyWord.Length;
                newTwitterUser.Followers = DeserializeFollowers(followersStartIndex, twitterUser);

                deserializedTwitterUsers.Add(newTwitterUser);
            }

            return deserializedTwitterUsers;
        }

        public IEnumerable<Tweet> DeserializeTweets(IEnumerable<string> userTweets)
        {
            var deserializedTweets = new List<Tweet>();

            if (userTweets == null)
            {
                // Tweets string is null.
                ConsoleLogger.SingleInstance.LogError("TextFileDeserializer.DeserializeTweets - Tweets string is null.");
                return deserializedTweets;
            }

            var delimterSetting = _appSettings["tweet.delimiter"];
            var delimiters = new[] { delimterSetting };
            var id = 0;

            foreach (var userTweet in userTweets)
            {
                if (string.IsNullOrWhiteSpace(userTweet))
                {
                    // Invalid user and tweet.
                    ConsoleLogger.SingleInstance.LogError("TextFileDeserializer.DeserializeTweets - Invalid user and tweet.");
                    continue;
                }

                var userTweetsSplit = userTweet.Trim().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

                if (userTweetsSplit.Length <= 0)
                {
                    // No User and Tweet found.
                    ConsoleLogger.SingleInstance.LogError("TextFileDeserializer.DeserializeTweets - No User and Tweet found.");
                    continue;
                }

                if (userTweetsSplit.Length == 1)
                {
                    // No Tweet found.
                    ConsoleLogger.SingleInstance.LogError("TextFileDeserializer.DeserializeTweets - No Tweet found.");
                    continue;
                }

                id++;
                var newTweet = new Tweet
                {
                    Id = id,
                    TwitterUser = new User
                    {
                        UserName = userTweetsSplit[0].Trim()
                    },
                    Message = DeSerializeTweet(userTweetsSplit[1])
                };

                deserializedTweets.Add(newTweet);
            }            

            return deserializedTweets;
        }

        // The user would be the start of the string until the key word "follows".
        private static User DeserializeUser(int indexOfKeyWord, string stringToDeserialize)
        {
            var user = new User();

            if (string.IsNullOrWhiteSpace(stringToDeserialize))
            {
                // Invalid string.
                ConsoleLogger.SingleInstance.LogError("TextFileDeserializer.DeserializeUser - Invalid user string.");
                return user;
            }

            if (indexOfKeyWord == 0)
            {
                // Invalid string. Cannot have follower without User.
                ConsoleLogger.SingleInstance.LogError("TextFileDeserializer.DeserializeUser - Invalid string. Cannot have follower without User.");
                return user;
            }

            if (indexOfKeyWord < 0)
            {
                // No key word.
                ConsoleLogger.SingleInstance.LogError("TextFileDeserializer.DeserializeUser - No key word found for users.");
                user.UserName = stringToDeserialize.Trim();
                return user;
            }

            var userName = stringToDeserialize.Substring(0, indexOfKeyWord);

            if (string.IsNullOrWhiteSpace(userName))
            {
                return user;
            }

            user.UserName = userName.Trim();

            return user;
        }

        private IEnumerable<User> DeserializeFollowers(int followersStartIndex, string stringToDeserialize)
        {
            var delimterSetting = _appSettings["followers.delimiter"];
            var delimiters = new[] { delimterSetting };
            var followers = new List<User>();

            if (string.IsNullOrWhiteSpace(stringToDeserialize))
            {
                // Invalid string.
                ConsoleLogger.SingleInstance.LogError("TextFileDeserializer.DeserializeFollowers - Invalid followers string.");
                return followers;
            }

            if (followersStartIndex <= 0)
            {
                // Invalid file. Cannot have follower without User.
                ConsoleLogger.SingleInstance.LogError("TextFileDeserializer.DeserializeFollowers - Invalid file. Cannot have follower without User.");
                return followers;
            }

            var twitterFollowers = stringToDeserialize.Substring(followersStartIndex, stringToDeserialize.Length - followersStartIndex);

            if (string.IsNullOrWhiteSpace(twitterFollowers))
            {
                // Empty followers.
                ConsoleLogger.SingleInstance.LogWarning("TextFileDeserializer.DeserializeFollowers - User has empty followers.");
                return followers;
            }

            var followersSplit = twitterFollowers.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            foreach (var follower in followersSplit)
            {
                followers.Add(new User
                {
                    UserName = follower.Trim()
                });
            }

            return followers;
        }

        private string DeSerializeTweet(string message)
        {
            var deserializedTweet = string.Empty;
            var maximumCharacters = Convert.ToInt32(_appSettings["tweet.maxlength"]);

            if (string.IsNullOrWhiteSpace(message))
            {
                // Message is invalid.
                ConsoleLogger.SingleInstance.LogError("TextFileDeserializer.DeSerializeTweet - Tweet message is invalid.");
                return deserializedTweet;
            }

            deserializedTweet = message.Trim();

            if (deserializedTweet.Length > maximumCharacters)
            {
                ConsoleLogger.SingleInstance.LogWarning($"TextFileDeserializer.DeSerializeTweet - Tweet message is greater than {maximumCharacters} charaters.");
                deserializedTweet = deserializedTweet.Substring(0, maximumCharacters);
            }

            return deserializedTweet;
        }
    }
}
