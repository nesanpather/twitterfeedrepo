﻿using System.Collections.Specialized;
using System.Configuration;
using Utilities.Interfaces;

namespace Utilities
{
    public class AppSettingsWrapper : IAppSettings
    {
        private readonly NameValueCollection _appSettings;

        public AppSettingsWrapper()
        {
            _appSettings = ConfigurationManager.AppSettings;
        }
        public string this[string key] => _appSettings[key];
    }
}
