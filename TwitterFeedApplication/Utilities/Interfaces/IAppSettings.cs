﻿namespace Utilities.Interfaces
{
    public interface IAppSettings
    {
        string this[string key] { get; }
    }
}
