﻿using System.Collections.Generic;
using TwitterFeedModels;

namespace Utilities.Interfaces
{
    public interface IDeserializer
    {
        IEnumerable<TwitterUser> DeserializeTwitterUsers(IEnumerable<string> twitterUsers);
        IEnumerable<Tweet> DeserializeTweets(IEnumerable<string> userTweets);
    }
}
