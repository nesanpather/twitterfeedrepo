﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataManager.Interfaces;
using TwitterFeedManager.Interfaces;
using TwitterFeedModels;
using Utilities;

namespace TwitterFeedManager
{
    public class TwitterFeedEngine : ITwitterFeed
    {
        private readonly IDataManager _dataManager;
        private readonly ITwitterUsersBuilder _twitterUsersBuilder;
        private readonly ITweetBuilder _tweetBuilder;

        public TwitterFeedEngine(IDataManager dataManager, ITwitterUsersBuilder twitterUsersBuilder, ITweetBuilder tweetBuilder)
        {
            _dataManager = dataManager;
            _twitterUsersBuilder = twitterUsersBuilder;
            _tweetBuilder = tweetBuilder;
        }

        public async Task<IEnumerable<TwitterFeed>> GetTwitterFeedAsync(string input1, string input2)
        {
            var twitterFeed = new List<TwitterFeed>();

            if (string.IsNullOrWhiteSpace(input1) || string.IsNullOrWhiteSpace(input2))
            {
                // Invalid file inputs.
                ConsoleLogger.SingleInstance.LogError("TwitterFeedEngine.GetTwitterFeedAsync - Invalid file inputs.");
                return twitterFeed;
            }

            var twitterUsers = await _dataManager.GetTwitterUsersAsync(input1);
            var enumerable = twitterUsers as IList<TwitterUser> ?? twitterUsers.ToList();
            var sortedUsersFollowers = PerformTwitterUsersOperations(enumerable);

            var tweets = await _dataManager.GetTweetsAsync(input2);
            var tweetsList = tweets as IList<Tweet> ?? tweets.ToList();

            foreach (var userFollower in sortedUsersFollowers)
            {
                var newUserFeed = new TwitterFeed { User = userFollower.User, Tweets = new List<Tweet>(), Followers = new List<User>() };
                var newUserTweetFeedList = new List<Tweet>();
                var newFollowerFeed = new List<User>();

                newUserTweetFeedList = AddTweet(tweetsList, userFollower.User, newUserTweetFeedList);

                newUserFeed.Tweets = newUserTweetFeedList;

                foreach (var follower in userFollower.Followers)
                {
                    newFollowerFeed.Add(follower);
                    var newFollowerTweetFeedList = new List<Tweet>();

                    newFollowerTweetFeedList = AddTweet(tweetsList, follower, newFollowerTweetFeedList);

                    if (newFollowerTweetFeedList.Count > 0)
                    {
                        newUserFeed.Tweets = newUserTweetFeedList.Concat(newFollowerTweetFeedList);
                    }
                }

                newUserFeed.Followers = newFollowerFeed;
                twitterFeed.Add(newUserFeed);
            }

            twitterFeed = _tweetBuilder.SortTweets(twitterFeed);

            return twitterFeed;
        }

        private IEnumerable<TwitterUser> PerformTwitterUsersOperations(IList<TwitterUser> enumerable)
        {
            var unionDuplicateUsers = _twitterUsersBuilder.UnionDuplicateUsers(enumerable);
            var sortedUsers = _twitterUsersBuilder.SortUsers(unionDuplicateUsers);
            var sortedUsersFollowers = _twitterUsersBuilder.SortFollowers(sortedUsers);
            return sortedUsersFollowers;
        }

        private static List<Tweet> AddTweet(IList<Tweet> tweetsList, User userFollower, List<Tweet> newUserTweetFeedList)
        {
            foreach (var tweet in tweetsList)
            {
                if (string.Equals(userFollower.UserName, tweet.TwitterUser.UserName, StringComparison.InvariantCultureIgnoreCase))
                {
                    newUserTweetFeedList.Add(new Tweet { TwitterUser = userFollower, Message = tweet.Message, Id = tweet.Id });
                }
            }

            return newUserTweetFeedList;
        }
    }
}
