﻿using System.Collections.Generic;
using TwitterFeedModels;

namespace TwitterFeedManager.Interfaces
{
    public interface ITwitterUsersBuilder
    {
        IEnumerable<TwitterUser> UnionDuplicateUsers(IEnumerable<TwitterUser> twitterUsers);
        IEnumerable<TwitterUser> SortUsers(IEnumerable<TwitterUser> twitterUsers);
        IEnumerable<TwitterUser> SortFollowers(IEnumerable<TwitterUser> twitterUsers);
    }
}
