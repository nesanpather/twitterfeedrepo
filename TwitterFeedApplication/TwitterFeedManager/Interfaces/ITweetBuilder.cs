﻿using System.Collections.Generic;
using TwitterFeedModels;

namespace TwitterFeedManager.Interfaces
{
    public interface ITweetBuilder
    {
        List<TwitterFeed> SortTweets(IEnumerable<TwitterFeed> twitterFeed);
    }
}
