﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TwitterFeedModels;

namespace TwitterFeedManager
{
    public interface ITwitterFeed
    {
        Task<IEnumerable<TwitterFeed>> GetTwitterFeedAsync(string input1, string input2);
    }
}
