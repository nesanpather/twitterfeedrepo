﻿using System;
using System.Collections.Generic;
using System.Linq;
using TwitterFeedManager.Interfaces;
using TwitterFeedModels;
using Utilities;

namespace TwitterFeedManager
{
    public class StandardTwitterUsersBuilder: ITwitterUsersBuilder
    {               
        public IEnumerable<TwitterUser> UnionDuplicateUsers(IEnumerable<TwitterUser> twitterUsers)
        {      
            var finalTwitterUsers = new List<TwitterUser>();

            if (twitterUsers == null)
            {
                ConsoleLogger.SingleInstance.LogError("StandardTwitterUsersBuilder.UnionDuplicateUsers - Twitter users are invalid.");
                return finalTwitterUsers;
            }

            var enumerable = twitterUsers as IList<TwitterUser> ?? twitterUsers.ToList();

            var query = enumerable.GroupBy(item => item.User.UserName)
                .Select(group => group.ToList())
                .ToList();
            
            foreach (var userFollower in query)
            {
                var firstOrDefault = userFollower.FirstOrDefault();

                if (firstOrDefault != null)
                {
                    var newTwitterUser = new TwitterUser
                    {
                        User = firstOrDefault.User,
                        Followers = new List<User>()
                    };
                    
                    foreach (var follower in userFollower)
                    {
                        // Union the followers.
                        newTwitterUser.Followers = newTwitterUser.Followers.Concat(follower.Followers);
                        // Remove duplicates.
                        newTwitterUser.Followers = LinqHelpers.DistinctBy(newTwitterUser.Followers, user => user.UserName).ToList();
                    }
                    
                    finalTwitterUsers.Add(newTwitterUser);
                }
            }
            
            return finalTwitterUsers;
        }

        public IEnumerable<TwitterUser> SortUsers(IEnumerable<TwitterUser> twitterUsers)
        {
            var sortUsers = twitterUsers as IList<TwitterUser> ?? twitterUsers.ToList();

            return sortUsers.OrderBy(o => o.User.UserName).ToList();
        }

        public IEnumerable<TwitterUser> SortFollowers(IEnumerable<TwitterUser> twitterUsers)
        {
            var sortFollowers = twitterUsers as IList<TwitterUser> ?? twitterUsers.ToList();

            foreach (var twitterUser in sortFollowers)
            {
                twitterUser.Followers = twitterUser.Followers.OrderBy(ordered => ordered.UserName);
            }

            return sortFollowers;
        }
    }
}
