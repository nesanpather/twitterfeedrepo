﻿using System.Collections.Generic;
using System.Linq;
using TwitterFeedManager.Interfaces;
using TwitterFeedModels;

namespace TwitterFeedManager
{
    public class StandardTweetBuilder: ITweetBuilder
    {
        public List<TwitterFeed> SortTweets(IEnumerable<TwitterFeed> twitterFeed)
        {
            var sortTweets = twitterFeed as IList<TwitterFeed> ?? twitterFeed.ToList();

            foreach (var feed in sortTweets)
            {
                feed.Tweets = feed.Tweets.OrderBy(ordered => ordered.Id);
            }

            return sortTweets.ToList();
        }
    }
}
