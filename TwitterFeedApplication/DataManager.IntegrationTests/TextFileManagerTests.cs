﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace DataManager.IntegrationTests
{
    [TestFixture]
    public class TextFileManagerTests
    {
        private string GetFullFilePathForIntegrationTest(string fileName)
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);

            if (!string.IsNullOrWhiteSpace(path))
            {
                var dir = Directory.GetParent(new UriBuilder(path).Path).Parent;
                var sb = new StringBuilder();
                if (dir != null) sb.Append(dir.FullName);
                sb.Append("\\");
                sb.Append(fileName);

                return sb.ToString();
            }

            return fileName;
        }

        [Test]
        public async Task GetFileContentsAsync_Given_ValidFullFileName_UserTextFile_Returns_IEnumerableString_NotNull()
        {
            // Arrange.
            var textFileManager = new TextFileManager();
            var fullFileName = "user.txt";
            fullFileName = GetFullFilePathForIntegrationTest(fullFileName);

            // Act.
            var actual = await textFileManager.GetFileContentsAsync(fullFileName);

            // Assert.
            Assert.IsNotNull(actual);
        }

        [Test]
        public async Task GetFileContentsAsync_Given_ValidFullFileName_UserTextFile_3Lines_Returns_IEnumerableString_Count3()
        {
            // Arrange.
            var textFileManager = new TextFileManager();
            var fullFileName = "user.txt";
            fullFileName = GetFullFilePathForIntegrationTest(fullFileName);

            // Act.
            var actual = await textFileManager.GetFileContentsAsync(fullFileName);

            // Assert.
            Assert.AreEqual(3, actual.Count());
        }

        [Test]
        public async Task GetFileContentsAsync_Given_ValidFullFileName_EmptyTextFile_Returns_IEnumerableString_Count0()
        {
            // Arrange.
            var textFileManager = new TextFileManager();
            var fullFileName = "empty.txt";
            fullFileName = GetFullFilePathForIntegrationTest(fullFileName);

            // Act.
            var actual = await textFileManager.GetFileContentsAsync(fullFileName);

            // Assert.
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.Count());
        }

        [Test]
        public async Task GetFileContentsAsync_Given_EmptyFullFileName_Returns_IEnumerableString_NotNull_Count0()
        {
            // Arrange.
            var textFileManager = new TextFileManager();
            var fullFileName = "";

            // Act.
            var actual = await textFileManager.GetFileContentsAsync(fullFileName);

            // Assert.
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.Count());
        }

        [Test]
        public async Task GetFileContentsAsync_Given_NullFullFileName_Returns_IEnumerableString_NotNull_Count0()
        {
            // Arrange.
            var textFileManager = new TextFileManager();

            // Act.
            var actual = await textFileManager.GetFileContentsAsync(null);

            // Assert.
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.Count());
        }

        [Test]
        public async Task GetFileContentsAsync_Given_FullFileName_DoesNotExist_Returns_IEnumerableString_NotNull_Count0()
        {
            // Arrange.
            var textFileManager = new TextFileManager();
            var fullFileName = "unknown.txt";
            fullFileName = GetFullFilePathForIntegrationTest(fullFileName);

            // Act.
            var actual = await textFileManager.GetFileContentsAsync(fullFileName);

            // Assert.
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.Count());
        }

        [Test]
        public async Task GetFileContentsAsync_Given_FullFileName_DifferentExtension_Returns_IEnumerableString_NotNull_Count0()
        {
            // Arrange.
            var textFileManager = new TextFileManager();
            var fullFileName = "user.png";
            fullFileName = GetFullFilePathForIntegrationTest(fullFileName);

            // Act.
            var actual = await textFileManager.GetFileContentsAsync(fullFileName);

            // Assert.
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.Count());
        }
    }
}
