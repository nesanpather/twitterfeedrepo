﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataManager;
using DataManager.Interfaces;
using TwitterFeedManager;
using TwitterFeedManager.Interfaces;
using TwitterFeedModels;
using Unity;
using Utilities;
using Utilities.Interfaces;

namespace TwitterFeedApplication
{
    class Program
    {
        static void Main(string[] args)
        {            
            var unityContainer = RegisterDependencies();
            var twitterFeedEngine = unityContainer.Resolve<ITwitterFeed>();
            
            var input1 = "user.txt";
            var input2 = "tweet.txt";

            if (args.Length == 0)
            {
                ConsoleLogger.SingleInstance.LogWarning("no input arguments found, defaulting to user.txt and tweet.txt");
            }

            if (args.Length >= 2)
            {
                if (!string.IsNullOrWhiteSpace(args[0]))
                {
                    input1 = args[0].Trim();
                }

                if (!string.IsNullOrWhiteSpace(args[1]))
                {
                    input2 = args[1].Trim();
                }
            }

            var twitterFeed = twitterFeedEngine.GetTwitterFeedAsync(input1, input2).GetAwaiter().GetResult();

            DisplayFeed(twitterFeed);

            Console.WriteLine();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        private static void DisplayFeed(IEnumerable<TwitterFeed> twitterFeed)
        {
            foreach (var feed in twitterFeed)
            {
                Console.WriteLine(feed.User.UserName);

                foreach (var tweet in feed.Tweets)
                {
                    Console.WriteLine("\t@{0}: {1}", tweet.TwitterUser.UserName, tweet.Message);
                }

                foreach (var follows in feed.Followers)
                {
                    Console.WriteLine(follows.UserName);
                }
            }
        }

        private static IUnityContainer RegisterDependencies()
        {
            IUnityContainer unityContainer = new UnityContainer();

            unityContainer.RegisterSingleton<ILogger, ConsoleLogger>();

            unityContainer.RegisterType<IAppSettings, AppSettingsWrapper>();
            unityContainer.RegisterType<IDeserializer, TextFileDeserializer>();
            unityContainer.RegisterType<IFileManager, TextFileManager>();
            unityContainer.RegisterType<IDataManager, TextDataManager>();
            unityContainer.RegisterType<ITwitterUsersBuilder, StandardTwitterUsersBuilder>();
            unityContainer.RegisterType<ITweetBuilder, StandardTweetBuilder>();
            unityContainer.RegisterType<ITwitterFeed, TwitterFeedEngine>();

            return unityContainer;
        }
    }
}
